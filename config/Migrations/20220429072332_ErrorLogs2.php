<?php
use Migrations\AbstractMigration;

class ErrorLogs2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('error_logs')
            ->addColumn('browser', 'string', ['default' => null, 'null' => true])
            ->addColumn('language', 'string', ['default' => null, 'null' => true, 'limit' => 16])
            ->addColumn('headers', 'text', ['default' => null, 'null' => true])
            ->update();
    }
}
