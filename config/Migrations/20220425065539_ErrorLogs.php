<?php

use Migrations\AbstractMigration;

class ErrorLogs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('error_logs')
            ->addColumn('file', 'string', ['default' => null, 'null' => true])
            ->addColumn('line', 'string', ['default' => null, 'null' => true, 'limit' => 6])
            ->addColumn('method', 'string', ['limit' => 16, 'default' => null, 'null' => true])
            ->addColumn('provider', 'string', ['limit' => 16, 'default' => null, 'null' => true])
            ->addColumn('md5', 'string', ['limit' => 32, 'default' => null, 'null' => true])
            ->addColumn('client_ip', 'string', ['limit' => 64, 'default' => null, 'null' => true])
            ->addColumn('environment', 'string', ['limit' => 64, 'default' => null, 'null' => true])
            ->addColumn('exception_class', 'string', ['default' => null, 'null' => true])
            ->addColumn('exception_url', 'text', ['default' => null, 'null' => true])
            ->addColumn('referer_url', 'text', ['default' => null, 'null' => true])
            ->addColumn('exception_message', 'text', ['default' => null, 'null' => true])
            ->addColumn('exception_code', 'text', ['default' => null, 'null' => true])
            ->addColumn('trace', 'text', ['default' => null, 'null' => true])
            ->addColumn('query', 'text', ['default' => null, 'null' => true])
            ->addColumn('data', 'text', ['default' => null, 'null' => true])
            ->addColumn('sent', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn('locked', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn('send_at', 'datetime', ['default' => null, 'null' => true])
            ->addColumn('send_tries', 'integer', ['default' => null, 'null' => true, 'limit' => 2])
            ->addColumn('created', 'datetime', ['default' => null])
            ->addIndex('sent')
            ->create();
    }
}
