<?php
namespace ErrorEmail\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use ErrorEmail\Model\Table\ErrorLogsTable;

/**
 * ErrorEmail\Model\Table\ErrorLogsTable Test Case
 */
class ErrorLogsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \ErrorEmail\Model\Table\ErrorLogsTable
     */
    public $ErrorLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.ErrorEmail.ErrorLogs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ErrorLogs') ? [] : ['className' => ErrorLogsTable::class];
        $this->ErrorLogs = TableRegistry::getTableLocator()->get('ErrorLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ErrorLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
