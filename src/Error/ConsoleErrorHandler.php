<?php
namespace ErrorEmail\Error;

use Cake\Console\ConsoleErrorHandler as CakeConsoleErrorHandler;
use ErrorEmail\Traits\EmailThrowableTrait;

/**
 * Extend cakes error handler to add email error functionality
 */
class ConsoleErrorHandler extends CakeConsoleErrorHandler
{
    use EmailThrowableTrait;
    use ErrorHandlerTrait;
}
