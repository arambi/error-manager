<?php
namespace ErrorEmail\Error;

use Cake\Error\ErrorHandler as CakeErrorHandler;
use ErrorEmail\Traits\EmailThrowableTrait;

/**
 * Extend cakes error handler to add email error functionality
 */
class ErrorHandler extends CakeErrorHandler
{
    use EmailThrowableTrait;
    use ErrorHandlerTrait;
}
