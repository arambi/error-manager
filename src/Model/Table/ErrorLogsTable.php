<?php
namespace ErrorEmail\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\I18n\FrozenTime;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Cake\Database\Schema\TableSchema;
use Cake\Database\Expression\QueryExpression;

/**
 * ErrorLogs Model
 *
 * @method \ErrorEmail\Model\Entity\ErrorLog get($primaryKey, $options = [])
 * @method \ErrorEmail\Model\Entity\ErrorLog newEntity($data = null, array $options = [])
 * @method \ErrorEmail\Model\Entity\ErrorLog[] newEntities(array $data, array $options = [])
 * @method \ErrorEmail\Model\Entity\ErrorLog|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ErrorEmail\Model\Entity\ErrorLog saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ErrorEmail\Model\Entity\ErrorLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \ErrorEmail\Model\Entity\ErrorLog[] patchEntities($entities, array $data, array $options = [])
 * @method \ErrorEmail\Model\Entity\ErrorLog findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ErrorLogsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('error_logs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function getBatch($size = 10)
    {
        return $this->getConnection()->transactional(function () use ($size) {
            $emails = $this->find()
                ->where([
                    $this->aliasField('sent') => false,
                    $this->aliasField('send_tries').' <=' => 3,
                    $this->aliasField('send_at').' <=' => new FrozenTime('now'),
                    $this->aliasField('locked') => false,
                ])
                ->limit($size)
                ->order([$this->aliasField('created') => 'ASC']);

            $emails
                ->extract('id')
                ->through(function ($ids) {
                    if (!$ids->isEmpty()) {
                        // $this->updateAll(['locked' => true], ['id IN' => $ids->toList()]);
                    }

                    return $ids;
                });

            return $emails->toList();
        });
    }

    /**
     * Releases locks for all emails in $ids.
     *
     * @param array|Traversable $ids The email ids to unlock
     */
    public function releaseLocks($ids)
    {
        $this->updateAll(['locked' => false], ['id IN' => $ids]);
    }

    /**
     * Releases locks for all emails in queue, useful for recovering from crashes.
     */
    public function clearLocks()
    {
        $this->updateAll(['locked' => false], '1=1');
    }

    /**
     * Marks an email from the queue as sent.
     *
     * @param string $id, queued email id
     *
     * @return bool
     */
    public function success($id)
    {
        $this->updateAll(['sent' => true], ['id' => $id]);
    }

    /**
     * Marks an email from the queue as failed, and increments the number of tries.
     *
     * @param string $id, queued email id
     *
     * @return bool
     */
    public function fail($id)
    {
        $this->updateAll(['send_tries' => new QueryExpression('send_tries + 1')], ['id' => $id]);
    }


    
    protected function _initializeSchema(TableSchema $schema)
    {
        $schema->setColumnType('data', 'json');
        $schema->setColumnType('query', 'json');
        return $schema;
    }
}
