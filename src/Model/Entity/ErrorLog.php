<?php
namespace ErrorEmail\Model\Entity;

use Cake\ORM\Entity;

/**
 * ErrorLog Entity
 *
 * @property int $id
 * @property string $sent
 * @property string|null $file
 * @property string|null $line
 * @property string|null $md5
 * @property string|null $client_ip
 * @property string|null $environment
 * @property string|null $exception_url
 * @property string|null $referer_url
 * @property string|null $exception_message
 * @property string|null $exception_code
 * @property string|null $trace
 * @property string|null $query
 * @property string|null $data
 * @property \Cake\I18n\Time $created
 */
class ErrorLog extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
