<?php
namespace ErrorEmail\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use ErrorEmail\Model\Entity\ErrorLog;

/**
 * Asana shell command.
 */
class AsanaShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser
            ->setDescription('Sends queued emails in a batch')
            ->addOption('limit', array(
                'short' => 'l',
                'help' => 'How many emails should be sent in this batch?',
                'default' => 50,
            ))
            ->addSubCommand('clearLocks', array(
                'help' => 'Clears all locked emails in the queue, useful for recovering from crashes',
            ))
            ->addSubCommand('user', array(
                'help' => 'Busca el id del usuario dado un nombre',
            ))
            ->addSubCommand('project', array(
                'help' => 'Busca el id del proyecto dado un nombre',
            ))
            ->addSubCommand('section', array(
                'help' => 'Busca el id de la sección y si no la encuentra, la crea',
            ))
            ->addSubCommand('task', array(
                'help' => 'Busca un task',
            ))
            ->addSubCommand('testError', array(
                'help' => 'Provoca un error para testeo',
            ));

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $errorLogs = $this->getTableLocator()->get('ErrorEmail.ErrorLogs');
        $logs = $errorLogs->getBatch($this->params['limit']);

        $count = count($logs);

        foreach ($logs as $log) {
            try {
                if ($this->canWriteTask($log)) {
                    $this->writeTask($log);
                } else {
                    $this->writeStory($log);
                }

                $sent = true;
            } catch (\Exception $exception) {
                $this->err($exception->getMessage());
                $sent = false;
            }

            if ($sent) {
                $errorLogs->success($log->id);
                $this->out('<success>Task '.$log->id.' was sent</success>');
            } else {
                $errorLogs->fail($log->id);
                $this->out('<error>Task '.$log->id.' was not sent</error>');
            }
        }

        if ($count > 0) {
            $errorLogs->releaseLocks(collection($logs)->extract('id')->toList());
        }
    }

    private function config($key)
    {
        return Configure::read("Asana.$key");
    }

    private function canWriteTask(ErrorLog $log)
    {
        $task = $this->findTask($log->md5);

        if ($task) {
            return false;
        }

        return true;
    }

    private function findTask($text)
    {
        $client = $this->getClient();
        $tasks = $client->tasks->searchTasksForWorkspace($this->config('workspace'), [
            'text' => $text,
            'completed' => false
        ]);

        foreach ($tasks as $task) {
            return $task;
        }
    }

    private function writeStory($log)
    {
        $client = $this->getClient();
        $task = $this->findTask($log->md5);
        $client->stories->createStoryForTask($task->gid, [
            'html_text' => "<body>" . $this->getLogHtmlStory($log) . "</body>"
        ]);
    }


    private function writeTask(ErrorLog $log)
    {
        $client = $this->getClient();
        $workspaceGid = $this->config('workspace');
        $data = [
            "approval_status" => "pending",
            "assignee_status" => "upcoming",
            'name' =>  $log->exception_message,
            'projects' => [$this->config('project')],
            'assignee' => $this->config('assignee'),
            'html_notes' => $this->getLogHtml($log),
            'workspace' => $workspaceGid,
            "completed" =>  false,
            'assignee_section' => $this->config('section'),
        ];

        if (!empty($this->config('followers'))) {
            $data['followers'] = $this->config('followers');
        }

        try {
            $task = $client->tasks->createTask($data);
            return $task;
        } catch (\Throwable $th) {
            _d( $th);
        }
    }

    public function section()
    {
        $client = $this->getClient();
        $sections = $client->sections->getSectionsForProject($this->config('project'));

        foreach ($sections as $section) {
            if ($section->name == $this->config('sectionName')) {
                $this->out($section->gid);
                die();
            }
        }

        $result = $client->sections->createSectionForProject($this->config('project'), [
            'name' => $this->config('sectionName'),
        ]);

        $this->out($result->gid);
    }

    private function getLogHtml($log)
    {
        $html = [
                "<code><strong>MD5:</strong> {$log->md5}</code>",
                "<code><strong>Environment:</strong> {$log->environment}</code>",
                "<code><strong>Exception Url:</strong> {$log->exception_url}</code>",
                "<code><strong>Referer Url:</strong> {$log->referer_url}</code>",
                "<code><strong>Exception Class:</strong> {$log->exception_class}</code>",
                "<code><strong>Exception Message:</strong> {$log->exception_message}</code>",
                "<code><strong>Exception Code:</strong> {$log->exception_code}</code>",
                "<code><strong>Client IP:</strong> {$log->client_ip}</code>",
                "<code><strong>User Agent:</strong> {$log->browser}</code>",
                "<code><strong>Language:</strong> {$log->language}</code>",
                "<code>In {$log->file} on line {$log->line}</code>",
                "<code><strong>Method:</strong> {$log->method}</code>",
                "<code><strong>Headers:</strong> {$log->headers}</code>",
                "<code><strong>Query:</strong></code>",
                "<code>". print_r($log->query, true) ."</code>",
                "<code><strong>Data:</strong></code>",
                "<code>". print_r($log->data, true) ."</code>",
                "<code><strong>Stack Trace:</strong></code>",
                implode("\n", array_map(function ($row) {
                    return "<code>$row</code>";
                }, explode("\n", $log->trace)))
            ];
        
        return "<body>". implode("\n", $html) ."</body>";
    }

    private function getLogHtmlStory($log)
    {
        $html = [
                "<code><strong>Exception Url:</strong> {$log->exception_url}</code>",
                "<code><strong>Referer Url:</strong> {$log->referer_url}</code>",
                "<code><strong>Client IP:</strong> {$log->client_ip}</code>",
                "<code><strong>User Agent:</strong> {$log->browser}</code>",
                "<code><strong>Language:</strong> {$log->language}</code>",
                "<code><strong>Method:</strong> {$log->method}</code>",
                "<code><strong>Headers:</strong> {$log->headers}</code>",
                "<code><strong>Query:</strong></code>",
                "<code>". print_r($log->query, true) ."</code>",
                "<code><strong>Data:</strong></code>",
                "<code>". print_r($log->data, true) ."</code>",
            ];
        
        return "<body>". implode("\n", $html) ."</body>";
    }


    private function getClient()
    {
        $client = \Asana\Client::accessToken(Configure::read('Asana.token'), [
            'log_asana_change_warnings' => false
        ]);

        return $client;
    }

    public function clearLocks()
    {
        $this->getTableLocator()->get('ErrorEmail')->clearLocks();
    }

    public function user()
    {
        $name = $this->in('Indica el nombre');
        $client = $this->getClient();

        foreach ($client->users->getUsers([
            'workspace' => $this->config('workspace')
        ]) as $user) {
            if ($user->name == $name) {
                $this->out($user->gid);
                die();
            }
        }

        $this->out('No se ha encontrado ningún usuario');
    }

    public function project()
    {
        $name = $this->in('Indica el nombre');
        $client = $this->getClient();
        $projects = $client->projects->getProjectsForWorkspace($this->config('workspace'));

        foreach ($projects as $project) {
            if ($project->name == $name) {
                $this->out($project->gid);
                die();
            }
        }

        $this->out('No se ha encontrado el projecto');
    }

    public function task()
    {
        $name = $this->in('Indica el nombre');
        $client = $this->getClient();

        $tasks = $client->tasks->searchTasksForWorkspace($this->config('workspace'), [
            'text' => $name,
        ]);

        foreach ($tasks as $task) {
            _d( $task);
            return false;
        }
    }
    

    public function testError()
    {
        $this->no->find();
    }
}
