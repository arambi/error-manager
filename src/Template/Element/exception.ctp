An exception has been thrown<?php if ($site) : ?> on <?= $site ?><?php endif; ?><?php if ($environment) : ?> (<?= $environment ?>)<?php endif; ?>

<?php if ($environment) : ?>
    <code><strong>Environment:</strong> <?= $environment ?></code>
<?php endif; ?>
<code><strong>Exception Url:</strong> <?= $this->Url->build($this->request->getAttribute('here'), true) ?></code>
<code><strong>Referer Url:</strong> <?= $this->request->referer() ?></code>
<code><strong>Exception Class:</strong> <?= get_class($exception) ?></code>
<code><strong>Exception Message:</strong> <?= $exception->getMessage() ?></code>
<code><strong>Exception Code:</strong> <?= $exception->getCode() ?></code>
<code><strong>Client IP:</strong> <?= $this->request->clientIp() ?></code>
<code>In <?= $exception->getFile() ?> on line <?= $exception->getLine() ?></code>
<code><strong>Stack Trace:</strong></code>
<?= implode("\n", array_map(function ($row) {
    return "<code>$row</code>";
}, explode("\n", $exception->getTraceAsString()))) ?>